const express = require('express'),
    bodyParser = require('body-parser');
    
const user = require('./routes/users'),
    team = require('./routes/team'),
    teamPlayers = require('./routes/teamPlayers'),
    fixtures = require('./routes/fixtures'),
    fixturePlayers = require('./routes/fixturePlayers'),
    globalErrHandler = require('./controllers/errorController'),
    AppError = require('./utils/appError'),
    app = express();

// Body parser, reading data from body into req.body
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

// Routes
app.use('/api/v1/home', (req, res, next) =>{
    res.render('welcome to my home page');
    next();
});
app.use('/api/v1/contant_us', (req, res, next) =>{
    res.render('contact us using the contacts below for more info');
    next();
});
app.use('/api/v1/team', team);
app.use('/api/v1/user', user);
app.use('/api/v1/teamPlayers', teamPlayers);
app.use('/api/v1/fixtures', fixtures);
app.use('/api/v1/fixturePlayers', fixturePlayers);

  // handle undefined Routes
app.use('*', (req, res, next) => {
    const err = new AppError(404, 'fail', 'undefined route');
    next(err, req, res, next);
});

// Start the server
const port = process.env.PORT || 5001;

app.listen(port, () => {  
  console.log(`Application is running on port ${port}`);
});

app.use(globalErrHandler);

module.exports = app;