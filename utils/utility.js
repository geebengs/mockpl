const bcrypt = require('bcryptjs');

exports.hashPassword = async (password) => {
  // generate a salt
  const salt = await bcrypt.genSalt(10);
  // hash the password along with our new salt
  const hash = await bcrypt.hash(password, salt);

  return hash;
};

exports.generateRandomCharacter = (length) => {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;

  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

exports.generateRandomPassword = () => {
    const password = generateRandomCharacter(6);

    return hashPassword(password);
}