const mongoose = require('mongoose'),
  session = require('express-session'),
  rateLimit = require('express-rate-limit'),
  helmet = require('helmet'),
  mongoSanitize = require('express-mongo-sanitize'),
  xss = require('xss-clean'),
  hpp = require('hpp'),
  cors = require('cors'),
  dotenv = require('dotenv');

dotenv.config({
    path: './config/dev.env'
});

const app = require('./app');

const database = process.env.MONGO_URI;

if (process.env.REDISTOGO_URL) {
  // TODO: redistogo connection
  var rtg   = require("url").parse(process.env.REDISTOGO_URL);
  var redis = require("redis").createClient(rtg.port, rtg.hostname);

  redis.auth(rtg.auth.split(":")[1]);

} else {
// connect redis
let RedisStore = require('connect-redis')(session);
let redisClient = redis.createClient();

// implementing session
app.use(
  session({
    store: new RedisStore({ 
      client: redisClient,
      ttl: process.env.SESSION_IDLE_TIMEOUT
      }),
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false
  })
);
}

// Connect the database
mongoose.connect(database, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true
}).then(con => {
  console.log('DB connection Successful!');
});

//handle lost connections to Redis
app.use(function (req, res, next) {
  if (!req.session) {
    return next(new Error('oh no')) // handle error
  }
  next() // otherwise continue
});

// Allow Cross-Origin requests
app.use(cors());

// Set security HTTP headers
app.use(helmet());

// Limit request from the same API 
const limiter = rateLimit({
  max: 150,
  windowMs: 60 * 60 * 1000,
  message: 'Too Many Request from this IP, please try again in an hour'
});
app.use('/api', limiter);

// Data sanitization against Nosql query injection
app.use(mongoSanitize());

// Data sanitization against XSS(clean user input from malicious HTML code)
app.use(xss());

// Prevent parameter pollution
app.use(hpp());

process.on('unhandledRejection', err => {
  console.log('UNHANDLED REJECTION!!!  shutting down ...');
  console.log(err.name, err.message);
});