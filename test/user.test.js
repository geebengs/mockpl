const request = require('supertest');
const app = require('../app');
const User = require('../models/users');

const user1 = {
    firstname: "glory",
    lastname: "glory",
    email: "collsophie7@yahoo.com",
    password: "goodness",
    passwordConfirm: "goodness",
    role: 1 
}

beforeEach(async() => {
   try {
    await User.deleteMany({})
    await User(user1).save();
   } catch (error) {
       return error
   } 
})

test('should signup for a user', async () =>{
    try {
    await request(app).post('/signup')
    .send({
        firstname: "glory",
        lastname: "glory",
        email: "collsophie747@yahoo.com",
        password: "goodness",
        passwordConfirm: "goodness",
        role: 1 
    })
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(200)
    } catch (error) {
        return error
    }
}, 30000);

test('should not sign up without complete fields', async() => {
    try {
        await request(app).post('/signup')
        .send({
            firstname: "glory",
            lastname: "glory"
        })
        .set('Accept', 'application/json')
        .expect(422)
    } catch (error) {
        return error
    }
}, 30000);

test('should not sign up without fields', async() => {
    try {
        await request(app).post('/signup')
        .send({})
        .set('Accept', 'application/json')
        .expect(422)
    } catch (error) {
        return error
    }
}, 30000);

test('should login with full detail', async() => {
    try {
        await request(app).post('/login')
        .send({
            email: user1.email,
            password: user1.password,
            role: user1.role 
        })
        .set('Accept', 'application/json')
        .expect(422)

    } catch (error) {
        return error
    }
}, 30000);

test('should get all users', async() => {
    try {
        await request(app).get('/')
        .expect([{firstname, lastname, email, password, role}])
    } catch (error) {
        return error
    }
}, 30000)
