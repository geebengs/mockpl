const request = require('supertest');
const app = require('../app');
const Fixtures = require('../models/fixtures');

const fixture1 = {
    date: "28/7/2020",
    teams: [{"team_id": "5f0de5b8ebf24436f09e71ee"}, {"team_id": "5f0de5b8ebf24436f09e71ed"}]
}

beforeEach(async() => {
    await Fixtures.deleteMany({})
    await Fixtures(fixture1).save();
})

test('should create a fixture', async () =>{
    try {
    await request(app).post('/fixtures/')
    .send({
        date: "27/7/2020",
        teams: [{"team_id": "5f0de5b8ebf24436f09e71dd"}, {"team_id": "5f0de5b8ebf24436f09e71de"}]
    })
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(200)
    } catch (error) {
        return error
    }
});