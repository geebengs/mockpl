const request = require('supertest');
const app = require('../app');
const Teams = require('../models/team');

const team1 = {
    "name": "chelsea",
    "coach": "john",
    "asst_coach": "ebuka"
}

beforeEach(async() => {
    await Teams.deleteMany({})
    await Teams(team1).save();
})

test('should create a team', async () =>{
    try {
    await request(app).post('/fixtures/')
    .send({
        "name": "chelsea",
        "coach": "john",
        "asst_coach": "ebuka"
    })
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(200)
    } catch (error) {
        return error
    }
});