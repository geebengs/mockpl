const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const teamPlayersSchema = new mongoose.Schema({
  team_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'team'
  },
  players: [
    {
      name: {
        type: String,
        require: true
      },
      position: {
        type: Number,
        require: true
      }
    }
  ]
},
{
    timestamps: { createdAt: true, updatedAt: false }
});

teamPlayersSchema.plugin(mongoosePaginate);
const TeamPlayers = mongoose.model('TeamPlayers', teamPlayersSchema);
module.exports = TeamPlayers;