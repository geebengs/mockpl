const mongoose = require('mongoose'),
  validator = require('validator'),
  mongoosePaginate = require('mongoose-paginate-v2'),
  bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema({
  firstname: {
    type: String,
    require: true
  },
  lastname: {
    type: String,
    require: true
  },
  email: {
    type: String,
    require: true,
    index: true,
    unique: true,
    lowercase: true,
    sparse: true,
    validate: [validator.isEmail, ' Please provide a valid email']
  },
  password: {
    type: String,
    require: true,
    minLength: 6,
    select: false
  },
  role: {
    type: Number,
    require: true,
    min: 1,
    max: 2,
    description: "1=admin, 2=user"
  },
  status: {
    type: Boolean
  },
  confirmation_token:{
    type: String
  },
  remember_token: {
    type: String
  }
},
{
    timestamps: { createdAt: true, updatedAt: false }
});

userSchema.pre('save', async function hashPassword(next) {
  try {
    const user = this;
  
    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();
  
    // generate a salt
    const salt = await bcrypt.genSalt(10);
  
    // hash the password along with our new salt
    const hash = await bcrypt.hash(user.password, salt);
  
    // override the cleartext password with the hashed one
    user.password = hash;

    return next();
  } catch (e) {
      return next(e);
  }
});

userSchema.statics = {
  isValid(id) {
    return this.findById(id)
      .then(result => {
        if (!result) throw new Error('User not found')
    })
  },
  emailExists(email) {
    return this.findOne({email})
      .then(result => {
        if (result) throw new Error('Email already exist')
      })
  }
};

// This is Instance Method that is gonna be available on all documents in a certain collection
userSchema.methods.correctPassword = async function (typedPassword, originalPassword) {
  return await bcrypt.compare(typedPassword, originalPassword);
};

userSchema.plugin(mongoosePaginate);
const User = mongoose.model('User', userSchema);
module.exports = User;