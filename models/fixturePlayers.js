const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const fixturePlayersSchema = new mongoose.Schema({
  fixture_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'team'
  },
  teams: [
    {
      team_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'team'
      },
      mainPlayers: [
        {
          name: {
            type: String,
            require: true
          },
          position: {
            type: Number,
            require: true
          }
        }
      ],
      substitute: [
        {
          name: {
            type: String,
            require: true
          },
          position: {
            type: Number,
            require: true
          }
        }
      ]
    }
  ]
},
{
    timestamps: { createdAt: true, updatedAt: false }
});

fixturePlayersSchema.pre('validate', function(next) {
  if (this.team.length !== 2) throw("team exceeds maximum array size (2)!");
  next();
});

fixturePlayersSchema.plugin(mongoosePaginate);
const FixturePlayersSchema = mongoose.model('FixturePlayersSchema', fixturePlayersSchema);
module.exports = FixturePlayersSchema;