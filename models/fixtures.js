const mongoose = require('mongoose'),
    mongoosePaginate = require('mongoose-paginate-v2');

const fixturesSchema = new mongoose.Schema({
  date: {
    type: String,
    require: true,
  },
  teams: [
    {
      team_id: {
        type: String,
        require: true
      },
      score: {
        type: Number,
        require: true,
        default: 0
      }
    }
  ],
  match_info: [
    {
      team_id: {
        type: String
      },
      player: {
        type: String
      },
      time: {
        type: String
      },
      event: {
        type: Number,
          min: 1,
          max: 5,
          description: '1=goal, 2=yellowCard, 3=redCard, 4=penalty, 5=offside'
      },        
    }
  ],
  referee: {
    type: String,
    require: true,
  },
  linesman1: {
    type: String,
    require: true,
  },
  linesman2: {
      type: String,
      require: true,
  },
  unique_id: {
    type: String,
    require: true,
  },
  status: {
    type: Number,
    min: 1,
    max: 3,
    default: 3,
    description: '1=completed, 2=pending, 3=started'
  }
},
{
    timestamps: { createdAt: true, updatedAt: false }
});

fixturesSchema.plugin(mongoosePaginate);
const Fixtures = mongoose.model('Fixtures', fixturesSchema);
module.exports = Fixtures;