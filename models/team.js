const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const teamSchema = new mongoose.Schema({
  name: {
    type: String,
    require: true
  },
  coach: {
    type: String,
    require: true
  },
  asst_coach: {
    type: String,
    require: true
  }
},
{
    timestamps: { createdAt: true, updatedAt: false }
});

teamSchema.plugin(mongoosePaginate);
const Team = mongoose.model('Team', teamSchema);
module.exports = Team;