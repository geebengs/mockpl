const express = require('express');
const { check, param } = require('express-validator');
const router = express.Router();
const teamPlayers = require('../controllers/teamPlayers');
const auth = require('../controllers/auth');

router.post('/', [
  check('team_id', 'input a valid mongo id').notEmpty().isMongoId(),
  check('players', 'input a valid name and position for the player').notEmpty().isArray().isLength({max:2, min:2}).withMessage('you have exceeded the maximum length')
], auth.protect, teamPlayers.createTeamPlayers);

router.get('/', auth.protect, teamPlayers.getAllTeamplayers);

router.get('/:id', [
  param('id', 'input a valid mongo id').notEmpty().isMongoId()
], auth.protect, teamPlayers.getOneTeamPlayer);

router.patch('/:id', [
  param('id', 'inout a valid mongo id').notEmpty().isMongoId(),
  check('id', 'input a valid mongo id').notEmpty().isMongoId(),
  check('team_id', 'input a valid mongo id').optional().isMongoId(),
  check('name', 'input a valid name for the player').optional().isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('position', 'input the valid position of the player').optional().isNumeric().isLength({max:30}).withMessage('you have exceeded the maximum number')
], auth.protect, teamPlayers.updateTeamPlayer);

router.delete('/:id', [
  param('id', 'input a valid mongo id').notEmpty().isMongoId()
], auth.protect, teamPlayers.deleteTeamPlayer);

router.search('/search', [
  check('query', 'input valid query field').isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('limit', 'input a valid limit').optional().isNumeric(),
  check('sort', 'input a valid sort').optional().isInt().isIn([1, -1])
], auth.protect, teamPlayers.searchTeamPlayer);

module.exports = router;