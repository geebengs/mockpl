const express = require('express');
const { check, param } = require('express-validator');
const router = express.Router();
const fixtures = require('../controllers/fixtures');
const auth = require('../controllers/auth');

router.post('/', auth.protect, [
  check('date').notEmpty(),
  check('teams').notEmpty().isArray().withMessage('input the team id and score').isLength({max: 2, min: 2}).withMessage('it must be a fixture of two teams')
], fixtures.createFixtures);

router.get('/', auth.protect,  fixtures.getAllFixtures);

router.get('/:id', [
  param('id', 'input a valid mongo id').notEmpty().isMongoId()
], auth.protect, fixtures.getOneFixture);

router.patch('/:id', [
  param('id', 'input a valid mongo id').notEmpty().isMongoId(),
  check('id', 'input a valid mongo id').notEmpty().isMongoId(),
  check('date', 'input the date of the fixture').optional().notEmpty().isDate(),
  check('referee', 'input the referee\'s name').optional().isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('linesman1', 'input the name of the first linesman').optional().isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('linesman2', 'input the name of the second linesman').optional().isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('status', 'input the status of the fixture').optional().isInt().isIn([1, 2, 3])
], auth.protect, fixtures.updateFixture);

router.patch('/update_match_info', [
  param('id', 'input a valid mongo id').notEmpty().isMongoId(),
  check('id', 'input a valid mongo id').notEmpty().isMongoId(),
  check('team_id', 'input a valid mongo id').optional().isMongoId(),
  check('player').optional().isString().isLength({ max:255 }).withMessage('you have exceeded the maximum length'),
  check('time', 'input the time of the fixture').optional().isString(),
  check('event').optional().isIn([1, 2, 3, 4, 5])
], auth.protect, fixtures.updateMatchInfo);

router.delete('/:id', [
  param('id', 'input a valid mongo id').notEmpty().isMongoId()
], auth.protect, fixtures.deleteFixture);

router.search('/search', [
  check('query', 'input valid query field').isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('limit', 'input a valid limit').optional().isNumeric(),
  check('sort', 'input a valid sort').optional().isInt().isIn([1, -1])
], fixtures.searchFixture);

module.exports = router;