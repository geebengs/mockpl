const express = require('express');
const router = express.Router();
const { check, param, query } = require('express-validator');
const user = require('../controllers/users');
const auth = require('../controllers/auth');
const Userschema = require('../models/users');

router.post('/signup', [
  check('firstname', 'input your firstname').notEmpty().isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('lastname', 'input your lastname').notEmpty().isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('email', 'input your email address').notEmpty().isEmail().custom(value => Userschema.emailExists(value)).isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('password', 'input your password').notEmpty().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('passwordConfirm', 'Passwords do not match').notEmpty().custom((value, {req}) => (value === req.body.password)),
  check('role', 'input your user role').notEmpty().isInt().isIn([1, 2]).isLength({min:1, max:2}).withMessage('you have exceeded the maximum length'),
], user.userSignup);

router.post('/login', [
  check('email', 'input your valid email').notEmpty().isEmail().withMessage('you have exceeded the maximum length'),
  check('password', 'input your valid password').notEmpty().isLength({max:255}).withMessage('you have exceeded the maximum length')
], user.userLogin);

router.get('/', user.getAllUsers);

router.get('/:id', [
  param('id', 'input a valid mongo id').notEmpty().isMongoId()
],  user.getOneUser);

router.get('/forgot_password', [
  query('email', 'Input an email address').notEmpty().isEmail().withMessage('You have inputted an invalid email address')
], auth.protect, user.forgot_password);

router.patch('/:id', [
    check('firstname', 'input your firstname').optional().isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
    check('lastname', 'input your lastname').optional().isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
    check('email', 'input your email address').optional().isEmail().isLength({max:255}).withMessage('you have exceeded the maximum length'),
    check('role', 'input your user role').optional().isInt().isIn([1, 2]).isLength({min:1, max:2}).withMessage('you have exceeded the maximum length'),
], auth.protect, user.updateUser);

router.patch('/change_password', [
  param('id', 'input a valid mongo id').notEmpty().isMongoId(),
  check('old_password', 'insert old password').notEmpty(),
  check('password', 'insert password').notEmpty().isLength({max:255}).withMessage('you have exceeded the maximum length')
  .isLength({ min: 6})        
    .custom((value,{req, loc, path}) => {
      if (value !== req.body.confirm_password) {
          // trow error if passwords do not match
          throw new Error("Passwords don't match");
      } else {
          return value;
      }
    })
], auth.protect, user.change_password)

router.patch('/confirm_account/:random_character', [
  param('random_character', 'input a valid mongo id').notEmpty().isUUID()
], auth.protect, user.confirm_account);

router.delete('/:id', [
  param('id', 'input a valid mongo id').notEmpty().isMongoId()
],  user.deleteUser);

router.search('/search', [
  check('query', 'input valid query field').isJSON(),
  check('limit', 'input a valid limit').optional().isNumeric(),
  check('sort', 'input a valid sort').optional().isInt().isIn([1, -1])
], auth.protect, user.searchUser);

module.exports = router;
