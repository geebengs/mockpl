const express = require('express');
const { check, param } = require('express-validator');
const router = express.Router();
const fixturePlayers = require('../controllers/fixturePlayers');
const auth = require('../controllers/auth');

router.post('/', [
  check('fixture_id', 'input a valid mongo id').notEmpty().isMongoId(),
  check('teams').optional().isArray().withMessage('input the team id, main players and the substitute players').isLength({max: 2, min: 2}).withMessage('it must be a fixture of two teams')
], auth.protect, fixturePlayers.createFixturePlayers);

router.get('/', auth.protect, fixturePlayers.getAllFixturePlayers);

router.get('/:id', [
  param('id', 'input a valid mongo id').notEmpty().isMongoId()
], auth.protect, fixturePlayers.getOneFixturePlayer);

router.patch('/:id', [
  param('id', 'input a valid mongo id').notEmpty().isMongoId(),
  check('id', 'input a valid mongo id').notEmpty().isMongoId(),
  check('teams').optional().isArray().withMessage('input the team id').isLength({max: 2, min: 2}).withMessage('it must be a fixture of two teams')
], auth.protect, fixturePlayers.updateFixturePlayer);

router.delete('/:id', [
  param('id', 'input a valid mongo id').notEmpty().isMongoId()
], auth.protect, fixturePlayers.deleteFixturePlayer);

router.search('/search', [
  check('query', 'input valid query field').isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('limit', 'input a valid limit').optional().isNumeric(),
  check('sort', 'input a valid sort').optional().isInt().isIn([1, -1])
], auth.protect, fixturePlayers.searchFixturePlayer);

module.exports = router;