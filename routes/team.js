const express = require('express');
const { check, param } = require('express-validator');
const router = express.Router();
const team = require('../controllers/team');
const auth = require('../controllers/auth');

router.post('/', [
  check('name', 'input the team name').notEmpty().isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('coach', 'input the coach name').notEmpty().isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('asst_coach', 'input the name of the assistant coach').notEmpty().isString().isLength({max:255}).withMessage('you have exceeded the maximum length')
], auth.protect, team.createTeam);

router.get('/', auth.protect, team.getAllTeams);

router.get('/:id', [
  param('id', 'input a valid mongo id').notEmpty().isMongoId()
], auth.protect, team.getOneTeam);

router.patch('/:id', [
  param('id', 'input valid mongo id').notEmpty().isMongoId(),
  check('id', 'input a valid mongo id').notEmpty().isMongoId(),
  check('name', 'input the team name').optional().isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('coach', 'input the coach name').optional().isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('asst_Coach', 'input the name of the assistant coach').optional().isString().isLength({max:255}).withMessage('you have exceeded the maximum length')
], auth.protect, team.updateTeam);

router.delete('/:id', [
  param('id', 'input a valid mongo id').notEmpty().isMongoId()
], auth.protect, team.deleteTeam);

router.search('/search', [
  check('query', 'input valid query field').isString().isLength({max:255}).withMessage('you have exceeded the maximum length'),
  check('limit', 'input a valid limit').optional().isNumeric(),
  check('sort', 'input a valid sort').optional().isInt().isIn([1, -1])
], auth.protect, team.searchTeam);

module.exports = router;