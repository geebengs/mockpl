const {promisify} = require('util'),
    User = require('../models/users'),
    user = require('./baseController'),
    jwt = require('jsonwebtoken'),
    base = require('./baseController'),
    mail = require('../services/mail'),
    { uuid } = require('uuidv4'),
    utility = require('../utils/utility'),
    {validationResult} = require('express-validator'),
    AppError = require('../utils/appError');

const createToken = id => {
  return jwt.sign({
    id
  }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN
  });
};

const generateToken = async () => {  
  try{
    const token = uuid();    
    const model = await User.findOne({confirmation_token: token});

    if(model) {
      return this.generateToken();
    }
    
    return token;
  }
  catch(error) {
    return error;
  }    
};


exports.userSignup = async (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  try {        
    const confirmationToken = await generateToken();  

    let body = req.body;
    body.status = req.body.role && req.body.role === 1 ? true: false;
    body.confirmation_token = confirmationToken; 
    
    const user = await User.create(body);
    
    await sendActivationEmail(req.body, user);

    //unset password
    user.password = undefined;
    
    return res.status(200).json({
      status: 'success',
      user               
    });
     
  } catch (error) {
    next(error);
  }

};

const sendActivationEmail = async(body, user) => {
  let emailBody = "";
  let subject = "";

  if(body.role === 1) {
    emailBody = '<p>Hello '+user.firstname+',</p><p> An admin account has just been created for you on Mock Premier League.</p><p> Login with the following details:</p><p>Url: '+process.env.BASE_URL+'/admin <br />Username: '+ user.email +'<br /> Password:'+body.password+'<p>Regard.<p> <p> Mock Premier League.</p>'; // HTML body
    subject = 'Mock Premier League Admin Account Created!';
  }
  else {
    const confirmationToken = await generateToken();  
    
    emailBody = '<p>Hello '+user.firstname+',</p><p> Welcome to Mock Premier League.</p><p>Kindly click on the link below to activate your account to have access to Mock Premier League which serves the latest scores of fixtures of matches.</p><p><a href="'+process.env.BASE_URL+'/user/confirm_account/'+confirmationToken+'">Accout Activation Link<a></p>';
    subject = 'Welcome to Mock Premier League!';
  }

  await mail.sendMail(user.email, subject, emailBody);
}

exports.userLogin = async (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  try {
    const {email, password, role} = req.body;

    // check if user exist and  password is correct
    const user = await User.findOne({email}).select('+password');

    if (!user || !await user.correctPassword(password, user.password)) {
      return next(new AppError(406, 'Not acceptable', 'Email/Password is incorrect'), req, res, next);
    }
       
    // check if user is active
    if(!user.status) {
      return next(new AppError(401, 'Unauthorized', 'You cannot access this account. See the administrator'), req, res, next);
    }
    
    //check if user is attempting to login as the correct role
    if(role !== user.role){
      return next(new AppError(403, 'fail', 'Email/Password is incorrect'), req, res, next);
    }
        
    // All correct, send jwt to client
    const token = createToken(user.id);
  
    // Implement session
    if(user) {
      req.session.userID = user._id

    // Remove the password from the output 
    user.password = undefined;
        
      res.status(200).json({
        status: 'success',
        token
      });
    }
  } catch (error) {
    next(error);
  }
};

exports.confirm_account = async(req, res, next) => {
  try{  
    const data = await User.findOneAndUpdate({confirmation_token: req.params.random_character}, {$set: {status: true}}, {
      new: true,
      runValidators: true,
    });

    if (!data) {
      return next(new AppError(404, 'fail', 'This confirmation token doesn\'t exist'), req, res, next);
    };

    const token = createToken(data.id); 

    res.status(200).json({
      status: 'your account has been activated successfully',
        data,
        token
    });
        
  }catch(err) {
    next(err);
  }
}

exports.forgot_password = async (req, res, next) =>{
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  try{
    const random_character = await generateToken();
    const user = await User.findOneAndUpdate({email:req.query.email}, {$set: {remember_token: random_character}}, {
      new: true,
      runValidators: true,
    });

    if(!user){
      return next(new AppError(404, 'Not found', 'The email does not exist'), req, res, next);
    };
        
    const emailBody = '<p>Hello '+user.firstname+',</p><p> You recently requested to change your password.</p><p> If you did not make this request, kindly ignore this email.</p><p>To change your password, click on the link below</p><p><a href="'+process.env.BASE_URL+'/user/reset_password/'+random_character+'">Reset Password Link<a></p>'// HTML body
    const subject =  'Password Reset Request' // Subject line
       
    // Mail User
    await mail.sendMail(user.email, subject, emailBody);

    res.status(200).json({
      status: 'A reset link has been sent to your email'
    });

  }catch(error){
    next(error);
  }
};

exports.reset_password = async (req, res, next) =>{
  const errors = validationResult(req);
  
  if(!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  };
  
  try{
    const hash = await utility.hashPassword(req.body.password);
    const data = await User.findOneAndUpdate({remember_token: req.params.random_character}, {$set: {password: hash}}, {
      new: true,
      runValidators: true,
    });
  
    if (!data) {
      return next(new AppError(404, 'Not found', 'Invalid token'), req, res, next);
    };
  
    res.status(200).json({
      status: 'your password reset was successful',
      data
    });
  }catch(error) {
    next(error);
  }
};

exports.change_password = async (req, res, next) =>{
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  };

  try{
    const user = await User.findById(req.body.id).select('+password');

    if (!user || !await user.correctPassword(req.body.old_password, user.password)) {
      return next(new AppError(409, 'Conflict', 'user id/old password is incorrect'), req, res, next);
    }

    const hash = await utility.hashPassword(req.body.password);
    const data = await User.findByIdAndUpdate( req.params.id, {$set: {password: hash}}, {
      new: true,
      runValidators: true,
    });

    if (!data) {
      return next(new AppError(404, 'Not found', 'No user was found with the id'), req, res, next);
    };

    const token = createToken(user.id);

    res.status(200).json({
      status: 'your password change was successful',
        data,
        token
    });
  }catch(error) {
    next(error);
  }
};

exports.updateUser = base.updateOne(User);
exports.getAllUsers = base.getAll(User);
exports.getOneUser = base.getOne(User);
exports.searchUser = base.search(User);
exports.deleteUser = base.deleteOne(User);