const base = require('../controllers/baseController');
const Fixtures = require('../models/fixtures');
const { validationResult } = require('express-validator');
const appConstants = require('../constants/app.constants');
const AppError = require('../utils/appError');
const { uuid } = require('uuidv4');

const generateToken = async () => {  
  try{
    const token = uuid();    
    const model = await Fixtures .findOne({confirmation_token: token});
    if(model) {
      return this.generateToken();
    }
  
    return token;
  }
  catch(error) {
    return error;
  }    
};

exports.updateMatchInfo = async(req, res, next) =>{
  const errors = validationResult(req);

  if(!errors.isEmpty()){
      return res.status(422).json({ errors: errors.array() });
  };
  if(req.params.id !== req.body._id) {
    return next(new AppError(406, 'Not acceptable', 'the route id does not match the supplied id'), req, res, next);
  };

  try {
    //insert new match info into fixture's match info collection
    const updated_fixture = await Fixtures.findByIdAndUpdate(req.params.id, {$push: {match_info: req.body}}, {new: true, runValidators: true});

    //if event being recorded is a goal then update scores
    if(req.body.event === appConstants.fixture_event.goal){
        const fixture = await updateScores(req.params.id, req.body.team_id);

        res.status(200).json({
          status: 'created',
          fixture
        })
    } 

    //return updated fixture as response
    res.status(200).json({
      status: 'created',
      updated_fixture
    })

  } catch (error) {
     next(error);
  }
}

const updateScores = async(fixture_id, team_id) => {
    const fixture = await findById(fixture_id);

    const updated_teams = fixture.teams.map(team => {
      return team.team_id === team_id ? team.score++ : team;      
    });

    const updated_fixture = await Fixtures.findByIdAndUpdate(fixture_id, {$set: {teams: updated_teams}}, {new: true, runValidators: true});

    return updated_fixture;
}

exports.createFixtures = async (req, res, next) =>{
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  };
  try {
    const unique_link = await generateToken();       

    let body = req.body;
        body.unique_id = unique_link;

    const data = await Fixtures.create(req.body);

    if(req.body.teams.length !== 2){
      return next( new AppError(422, 'unprocessible', 'The teams must be an array of two teams'))
    };

    if(!data) {
      return next(new AppError(404, 'Not found', 'The id you supplied doesn\'t exist'), req, res, next)
    };    
    
    res.status(200).json({
        status: 'success',
          data
      });
      
  } catch (error) {
    next(error);
  }
};

exports.updateFixture = base.updateOne(Fixtures);
exports.getAllFixtures = base.getAll(Fixtures);
exports.getOneFixture = base.getOne(Fixtures);
exports.deleteFixture = base.deleteOne(Fixtures);
exports.searchFixture = base.search(Fixtures);