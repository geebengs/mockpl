const team = require('../models/team');
const base = require('../controllers/baseController');

exports.createTeam = base.createOne(team);
exports.getOneTeam = base.getOne(team);
exports.getAllTeams = base.getAll(team);
exports.updateTeam = base.updateOne(team);
exports.deleteTeam = base.deleteOne(team);
exports.searchTeam = base.search(team);