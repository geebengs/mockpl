const fixturePlayers = require('../models/fixturePlayers');
const base = require('../controllers/baseController');
const {validationResult} = require('express-validator');

exports.createFixturePlayers = async (req, res, next) =>{
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  };
  try {
    const data = await fixturePlayers.create(req.body);

    if(req.body.teams.length !== 2){
      return next( new AppError(422, 'unprocessible', 'The teams must be an array of two teams'))
    };

    if(!data) {
      return next(new AppError(404, 'Not found', 'The id you supplied doesn\'t exist'), req, res, next)
    };    
    
    res.status(200).json({
        status: 'success',
          data
      });
      
  } catch (error) {
    next(error);
  }
};

exports.getOneFixturePlayer = base.getOne(fixturePlayers);
exports.getAllFixturePlayers = base.getAll(fixturePlayers);
exports.updateFixturePlayer = base.updateOne(fixturePlayers);
exports.deleteFixturePlayer = base.deleteOne(fixturePlayers);
exports.searchFixturePlayer = base.search(fixturePlayers);