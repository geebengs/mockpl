const AppError = require('../utils/appError');
const { validationResult} = require('express-validator');

exports.createOne = Model => async (req, res, next) =>{
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  };
  try {
    const data = await Model.create(req.body);
    
    if(!data) {
      return next(new AppError(404, 'Not found', 'The id you supplied doesn\'t exist'), req, res, next)
    };    
    
    res.status(200).json({
        status: 'success',
          data
      });
      
  } catch (error) {
    next(error);
  }
};

exports.getOne = Model => async (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  };
  try {
    const data = await Model.findById(req.params.id);

    if(!data) {
      return next(new AppError(404, 'Not found', 'The id you supplied doesn\'t exist'), req, res, next)
    };

    res.status(200).json({
      status: 'success',
      data
    });
  } catch (error) {
    next(error);
  }
};

exports.getAll = Model => async (req, res, next) => {
  try {
    const data = await Model.find();

    res.status(200).json({
      status: 'success',
      data
    });
  } catch (error) {
      next(error);
  }

};

exports.updateOne = Model => async (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  if(req.params.id !== req.body._id) {
    return next(new AppError(406, 'Not acceptable', 'the route id does not match the supplied id'), req, res, next);
  };

  try {
    const update = await Model.findByIdAndUpdate(req.params.id, { $set: req.body}, {new: true, runValidators: true });

    if(!update){
      return next (new AppError(404, 'Not found', 'This Document doesn\'t exist'), req, res, next)
    };

    res.status(200).json({
      status: 'updated',
      update
    })
  } catch (error) {
    next(error);
  }
};

exports.deleteOne = Model => async (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  };
  try {
    const doc = await Model.findById(req.params.id)
        
      if (!doc) {
        return next(new AppError(404, 'Not found', 'No document found with that id'), req, res, next);
      }        

      await Model.findByIdAndDelete(req.params.id);

      res.status(200).json({
        status: 'This document has been deleted'
      });  
  } catch (error) {
    next(error);
  }
};

exports.search = (Model, config=null) => async (req, res, next) => {
  try {
    var page = (req.body.page) ? req.body.page : 1;
    var perPage = (req.body.limit) ? req.body.limit :10;
    var query = req.body.query || {};
        
    var options = {
      sort: req.body.sort || {createdAt: -1},            
      lean: true,
      page: page, 
      limit: perPage
    };

      if(config && config.populate) {
        options.populate = config.populate;
      }
        
    const data = await Model.paginate(query, options); 

    res.status(200).json({
      status: 'success',
        data
    });

  } catch (error) {
     next(error);
  }
};