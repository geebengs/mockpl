const teamPlayers = require('../models/teamPlayers');
const base = require('../controllers/baseController');
const { validationResult } = require('express-validator');

exports.createTeamPlayers = async(req, res, next) => {
  const errors = validationResult(req);

  if(!error.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  };

  try {
      let body = req.body;
      
      body.players = {
        name: req.body.name,
        position: req.body.position
      };
    
      const players = await teamPlayers.create(body);

      res.status(200).json({
        status: 'Created the Team Players',
        players
      });
  } catch (error) {
    next(error);
  }
};

exports.getOneTeamPlayer = base.getOne(teamPlayers);
exports.getAllTeamplayers = base.getAll(teamPlayers);
exports.updateTeamPlayer = base.updateOne(teamPlayers);
exports.deleteTeamPlayer = base.deleteOne(teamPlayers);
exports.searchTeamPlayer = base.search(teamPlayers);